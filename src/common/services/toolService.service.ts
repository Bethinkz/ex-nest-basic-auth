import { Injectable, HttpStatus } from '@nestjs/common';

import { Response } from 'express';

@Injectable()
export class ToolService {
  constructor() {}

  makeRandom(lengthOfCode: number = 10) {
    const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    let text = '';
    for (let i = 0; i < lengthOfCode; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
  }

  getDateFromDateObject(date: Date): string {
    const month = date.getUTCMonth() + 1;
    const day = date.getUTCDate();
    const year = date.getUTCFullYear();
    const newdate = year + '/' + month + '/' + day;
    return newdate;
  }

  formSuccessResponse(resp: Response, data) {
    return resp.status(HttpStatus.OK).send(data);
  }

  formErrorResponse(
    resp: Response,
    errorDesc = 'ไม่สามารถระบุได้ กรุณาติดต่อ Administrator',
  ) {
    const responseText = {
      errCode: HttpStatus.BAD_REQUEST,
      errDesc: errorDesc,
    };
    return resp.status(HttpStatus.BAD_REQUEST).send(responseText);
  }

  formErrorUnauthorizedResponse(
    resp: Response,
    errorDesc = 'ไม่สามารถระบุได้ กรุณาติดต่อ Administrator',
  ) {
    const responseText = {
      errCode: HttpStatus.UNAUTHORIZED,
      errDesc: errorDesc,
    };
    return resp.status(HttpStatus.BAD_REQUEST).send(responseText);
  }
  formInternalServerErrorResponse(
    resp: Response,
    errorDesc = 'ไม่สามารถระบุได้ กรุณาติดต่อ Administrator',
  ) {
    const responseText = {
      errCode: HttpStatus.INTERNAL_SERVER_ERROR,
      errDesc: errorDesc,
    };
    return resp.status(HttpStatus.INTERNAL_SERVER_ERROR).send(responseText);
  }

}
