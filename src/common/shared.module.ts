import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { PassportModule } from '@nestjs/passport';

import { ToolService } from './services/toolService.service';
import { BasicStrategy } from './services/auth-basic.strategy';

@Module({
  imports: [
    PassportModule,
    ConfigModule.forRoot({
      //envFilePath: './environment/.master.env',
      envFilePath: './environment/.develop.env',
      isGlobal: true,
    }),
  ],
  providers: [ToolService, BasicStrategy],
  exports: [ToolService, BasicStrategy],
})
export class SharedModule {}
