import {
  Module,
  NestModule,
  MiddlewareConsumer,
  RequestMethod,
} from '@nestjs/common';

import { AppController } from './app.controller';
import { AppService } from './app.service';

import { SharedModule } from './common/shared.module';
import { SS021Module } from './ss021/ss021.module';

import { LoggerMiddleware } from './middleware/logger.middleware';
@Module({
  imports: [SS021Module, SharedModule],
  controllers: [AppController],
  providers: [AppService],
  exports: [SharedModule],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(LoggerMiddleware)
      .forRoutes({ path: '', method: RequestMethod.ALL });
  }
}
