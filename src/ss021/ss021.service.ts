import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository, InjectConnection } from '@nestjs/typeorm';
import { Repository, SimpleConsoleLogger, Connection } from 'typeorm';

@Injectable()
export class SS021Service {
	constructor(
		@InjectConnection('newcc')
		private readonly newccRepository: Connection,
	) {}

    async viewInsuranceCompany(): Promise<any> {
		const rawData = await this.newccRepository.query(
			"usp_InsuranceCompanyRef_VIEW @CmdType='0',@Code=''"
		);
    return  rawData;
	}

	async viewDeduct(AccCallNo): Promise<any> {
		const rawData = await this.newccRepository.query(`usp_GET_Deduct @AccCallNo='${AccCallNo}',@AccPersonNo='0'`);

		return rawData[0];
	}

	async viewPolicyInfo(AccCallNo): Promise<any> {
		const rawData = await this.newccRepository.query(`usp_ws_PolicyInfo @AccCallno='${AccCallNo}'`);
		rawData.forEach((res, index) => {
			const formData = {
				AccCallNo: res.AccCallNo,
				CustTitleId: res.CustTitleId,
				CustTitleDesc: res.CustTitleDesc,
				CustName: res.CustName,
				CustAddrNo: res.CustAddrNo,
				CustAddrMu: res.CustAddrMu,
				CustAddrVillage: res.CustAddrVillage,
				CustAddrSoi: res.CustAddrSoi,
				CustAddrRoad: res.CustAddrRoad,
				CustAddrProvinceId: res.CustAddrProvinceId,
				CustAddrProvinceName: res.CustAddrProvinceName,
				CustAddrAmphurId: res.CustAddrAmphurId,
				CustAddrAmphurName: res.CustAddrAmphurName,
				CustAddrTambolId: res.CustAddrTambolId,
				CustAddrTambolName: res.CustAddrTambolName,
				CustAddrPostCode: res.CustAddrPostCode,
			};
			rawData[index] = formData;
		});

		return rawData;
	}

}
