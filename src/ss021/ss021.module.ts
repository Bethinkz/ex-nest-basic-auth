import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { SharedModule } from 'src/common/shared.module';

import { SS021Controller } from './ss021.controller';

import { SS021Service } from './ss021.service';
@Module({
  imports: [
    SharedModule,
    TypeOrmModule.forRoot({
      name: 'newcc',
      type: 'mssql',
      host: process.env.SERVER_NEWCC_IP,
      port: Number(process.env.SERVER_NEWCC_PORT) || 1433,
      username: process.env.SERVER_NEWCC_USERNAME,
      password: process.env.SERVER_NEWCC_PASSWORD,
      database: process.env.SERVER_NEWCC_DATABASE_NAME,
      entities: [],
      synchronize: true,
      options: {
        encrypt: false,
        enableArithAbort: true,
      },
      autoLoadEntities: true,
      retryAttempts: 1000,
    }),
    TypeOrmModule.forFeature([], 'newcc'),
  ],
  controllers: [SS021Controller],
  providers: [SS021Service],
  exports: [SS021Service],
})
export class SS021Module {}
