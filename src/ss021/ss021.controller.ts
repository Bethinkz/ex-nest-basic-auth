import { Controller, Get, Post, Body, Res, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Response } from 'express';

import { SS021Service } from './ss021.service';
import { ToolService } from '../common/services/toolService.service';

@Controller('ss021')
export class SS021Controller {
  constructor(
    private ss021Service: SS021Service,
    private toolService: ToolService,
  ) {}

  @Get('insuranceCompany')
  @UseGuards(AuthGuard('basic'))
  async getInsuranceCompany(@Body() body, @Res() response: Response) {
    try {
      let packageData: any = [];
      packageData = await this.ss021Service.viewInsuranceCompany();
      return this.toolService.formSuccessResponse(response, packageData);
    } catch (err) {
      this.toolService.formErrorResponse(response, err.message);
    }
  }

  @Post('getDeduct')
  async getDeduct(@Body() body, @Res() response: Response) {
    try {
      let packageData: any = [];
      packageData = await this.ss021Service.viewDeduct(body.AccCallNo);
      return this.toolService.formSuccessResponse(response, packageData);
    } catch (err) {
      this.toolService.formErrorResponse(response, err.message);
    }
  }

  @Post('getPolicyInfo')
  async getPolicyInfo(@Body() body, @Res() response: Response) {
    try {
      let packageData: any = [];
      packageData = await this.ss021Service.viewPolicyInfo(body.AccCallNo);
      return this.toolService.formSuccessResponse(response, packageData);
    } catch (err) {
      this.toolService.formErrorResponse(response, err.message);
    }
  }
}
